﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI1212255.Models
{
    interface IUserRepository
    {
        IEnumerable<User> GetAll();
        User Get(int id);
        User GetUsername(string usename);
        User Add(User item);
        void Remove(string username);
        bool Update(User item);
    }
}
