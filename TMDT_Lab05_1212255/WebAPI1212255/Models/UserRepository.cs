﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI1212255.Models
{
    public class UserRepository : IUserRepository
    {
        private List<User> users = new List<User>();
        private int _nextId = 1;

        public UserRepository()
        {
            Add(new User { Id = 1, Username = "1212255", Password = "255", Email = "1212255@student.hcmus.edu.vn" });
            Add(new User { Id = 2, Username = "1212244", Password = "244", Email = "1212244@student.hcmus.edu.vn" });
            Add(new User { Id = 3, Username = "1212245", Password = "245", Email = "1212245@student.hcmus.edu.vn" });
        }
        public User Add(User item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            users.Add(item);
            return item;
        }

        public User Get(int id)
        {
            return users.Find(p => p.Id == id);
        }
        public User GetUsername(string username)
        {
            return users.Find(p => p.Username == username);
        }

        public IEnumerable<User> GetAll()
        {
            return users;
        }

        public void Remove(string username)
        {
            users.RemoveAll(p => p.Username == username);
        }

        public bool Update(User item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = users.FindIndex(p => p.Username == item.Username);
            if (index == -1)
            {
                return false;
            }
            users.RemoveAt(index);
            users.Add(item);
            return true;
        }
    }
}