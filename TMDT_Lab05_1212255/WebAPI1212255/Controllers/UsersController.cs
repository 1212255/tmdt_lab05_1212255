﻿using WebAPI1212255.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebAPI1212255.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {
        static readonly IUserRepository repository = new UserRepository();

        [Route("api/2.0/users")]
        public IEnumerable<User> GetAllUsers1212255()
        {
            return repository.GetAll();
        }
        [Route("api/2.0/users/{username}")]
        public User GetUser1212255(string username)
        {
            User item = repository.GetUsername(username);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }

        //public IEnumerable<User> GetProductsByCategory(string category)
        //{
        //    return repository.GetAll().Where(
        //        p => string.Equals(p.Category, category, StringComparison.OrdinalIgnoreCase));
        //}


        [Route("api/2.0/users/")]
        public User PostUser1212255([FromBody]User item)
        {
            item = repository.Add(item);
            return item;
        }

        //public httpresponsemessage postproduct(product item)
        //{
        //    item = repository.add(item);
        //    var response = request.createresponse<product>(httpstatuscode.created, item);

        //    string uri = url.link("defaultapi", new { id = item.id });
        //    response.headers.location = new uri(uri);
        //    return response;
        //}

        //update
        [Route("api/2.0/users/{username}")]
        public void PutUser1212255(string username, User user)
        {
            user.Username = username;
            if (!repository.Update(user))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }



        //delete
        [Route("api/2.0/users/{username}")]
        public void DeleteProduct1212255(string username)
        {
            User item = repository.GetUsername(username);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            repository.Remove(username);
        }
    }
}
